package com.example.myapplication;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

public class ListNewsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ListNewsAdapter adapter;


    private SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_news);

        recyclerView = findViewById(R.id.recyclerview);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ListNewsViewModel model = ViewModelProviders.of(this).get(ListNewsViewModel.class);

        model.getNews().observe(this, new Observer<List<ListnewsModel>>() {
            @Override
            public void onChanged(@Nullable List<ListnewsModel> newsList) {
                adapter = new ListNewsAdapter(ListNewsActivity.this, newsList);
                recyclerView.setAdapter(adapter);
            }
        });
    }
}
