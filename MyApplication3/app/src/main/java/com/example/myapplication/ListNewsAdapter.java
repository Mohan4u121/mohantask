package com.example.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import static android.view.LayoutInflater.from;

public class ListNewsAdapter extends RecyclerView.Adapter<ListNewsAdapter.ListNewsHolder> {

    Context context;

    List<ListnewsModel> listnewsModelList;


    public  ListNewsAdapter(Context context,List<ListnewsModel> listnewsModelList){
        this.context=context;
        this.listnewsModelList=listnewsModelList;

    }

    @NonNull
    @Override
    public ListNewsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_layout, viewGroup, false);
        return new ListNewsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListNewsHolder listNewsHolder, int i) {

        ListnewsModel Listnews=listnewsModelList.get(0);

        Glide.with(context)
                .load(Listnews.getPayload().get(i).getImageX())
                .into(listNewsHolder.imageView);
        listNewsHolder.title.setText(Listnews.getPayload().get(i).getTitleX());
        listNewsHolder.description.setText(Listnews.getPayload().get(i).getDescription());
        listNewsHolder.date.setText(Listnews.getPayload().get(i).getDateX());

    }

    @Override
    public int getItemCount() {
        return listnewsModelList.get(0).getPayload().size();
    }

    public class ListNewsHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView title,description,date;

        public ListNewsHolder(@NonNull View itemView) {
            super(itemView);
            imageView=(ImageView)itemView.findViewById(R.id.imageView);
            title=(TextView) itemView.findViewById(R.id.title);
            description=(TextView) itemView.findViewById(R.id.description);
            date=(TextView) itemView.findViewById(R.id.date);

        }
    }

    public void clear() {
        listnewsModelList.clear();
        notifyDataSetChanged();
    }

    // Add a list of items -- change to type used
    public void addAll(List<ListnewsModel> list) {

        listnewsModelList.addAll(list);
        notifyDataSetChanged();
    }
}
