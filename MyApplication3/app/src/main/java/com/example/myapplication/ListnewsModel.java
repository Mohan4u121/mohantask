package com.example.myapplication;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListnewsModel {

    /**
     * payload : [{"title":"Mohammed bin Rashid inspects infrastructure projects in the eastern region","description":"Mohammed bin Rashid inspects infrastructure projects in the eastern region","date":"02-Mar-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"},{"title":"Mohammed bin Rashid Housing permit grants for bachelor dwellings","description":"Mohammed bin Rashid Housing permit grants for bachelor dwellings","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"},{"title":"Smart Engineering Advisor application for housing grants launched","description":"Smart Engineering Advisor application for housing grants launched","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/2016-01-21.jpg"},{"title":"Mohammed bin Rashid Housing receive development ideas through smart Council","description":"Mohammed bin Rashid Housing receive development ideas through smart Council","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/Sami.jpg"},{"title":"Mohammed bin Rashid Housing hand over 453 houses to citizens in 2015","description":"Mohammed bin Rashid Housing hand over 453 houses to citizens in 2015","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"},{"title":"Mohammed bin Rashid Housing completed 492 residential land grant study","description":"Mohammed bin Rashid Housing completed 492 residential land grant study","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/Sami.jpg"},{"title":"Approval of 2800 housing assistances in 2015","description":"Approval of 2800 housing assistances in 2015","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/Sami.jpg"},{"title":"Mohammed bin Rashid Housing to spend 54 billion in 8 years","description":"Mohammed bin Rashid Housing to spend 54 billion in 8 years","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/Sami.jpg"},{"title":"Sami Gargash Exceptional model of leadership","description":"Sami Gargash Exceptional model of leadership","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/Sami.jpg"},{"title":"Hamdan Bin Mohammed University implemented Smart Diploma Leadership","description":"Hamdan Bin Mohammed University implemented Smart Diploma Leadership","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/2016-01-04.jpg"},{"title":"Postpone monthly deduction to ease the financial burden","description":"Postpone monthly deduction to ease the financial burden","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/Sami.jpg"},{"title":"Mohammed Bin Rashid Housing Establishment conclude National Day events by the launch of the martyrs electronic magazine","description":"Mohammed Bin Rashid Housing Establishment conclude National Day events by the launch of the martyrs electronic magazine","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/2015-11-30.jpg"},{"title":"Mohammed Bin Rashid Housing Establishment defer deductions loans 18 months","description":"Mohammed Bin Rashid Housing Establishment defer deductions loans 18 months","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/Sami.jpg"},{"title":"Electronic committee to speed decision on the granting of residential land in Dubai","description":"Electronic committee to speed decision on the granting of residential land in Dubai","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"},{"title":"Mohammed Bin Rashid Housing taught installments to beneficiaries prefabricated housing","description":"Mohammed Bin Rashid Housing taught installments to beneficiaries prefabricated housing","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"},{"title":"Dubai Excellence reviews the projects","description":"Dubai Excellence reviews the projects","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/2015-11-11.jpg"},{"title":"Mohammed Bin Rashid Housing Establishment looked at the social housing system in Turkey","description":"Mohammed Bin Rashid Housing Establishment looked at the social housing system in Turkey","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/2015-11-12.jpg"},{"title":"5 billion dirhams value of the loan portfolio of the Mohammed bin Rashid Housing","description":"5 billion dirhams value of the loan portfolio of the Mohammed bin Rashid Housing","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"},{"title":"Mohammed Bin Rashid Housing Establishment completes its preparations to celebrate the 44th National Day","description":"Mohammed Bin Rashid Housing Establishment completes its preparations to celebrate the 44th National Day","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"},{"title":"Reflections on the election","description":"Reflections on the election","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/Sami.jpg"},{"title":"Mohammed Bin Rashid Housing Establishment is participating in GITEX 2015","description":"Mohammed Bin Rashid Housing Establishment is participating in GITEX 2015","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"},{"title":"Mohammed bin Rashid Housing launch 4 smart applications to serve beneficiaries","description":"Mohammed bin Rashid Housing launch 4 smart applications to serve beneficiaries","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/2015-10-26.jpg"},{"title":"Mohammed bin Rashid Housing guide the energy consumption of its facilities","description":"Mohammed bin Rashid Housing guide the energy consumption of its facilities","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/Burhan.jpg"},{"title":"Mohammed Bin Rashid Housing Establishment finished design of five new projects","description":"Mohammed Bin Rashid Housing Establishment finished design of five new projects","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"},{"title":"Noha Kiswani Will and Ambition are the leading human capabilities","description":"Noha Kiswani Will and Ambition are the leading human capabilities","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/2015-08-22.jpg"},{"title":"Mohammed Bin Rashid Establishment issued approvals for 284 residential and 690 land grants during the first half","description":"Mohammed Bin Rashid Establishment issued approvals for 284 residential and 690 land grants during the first half","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"},{"title":"The conclusion of the summer camp in the Mohammed bin Rashid Housing","description":"The conclusion of the summer camp in the Mohammed bin Rashid Housing","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"},{"title":"Mohammed Bin Rashid Housing Establishment organizes heritage identity","description":"Mohammed Bin Rashid Housing Establishment organizes heritage identity","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/Hitham.jpg"},{"title":"A lecture on the ten days of Ramadan virtues by Mohammed bin Rashid Housing","description":"A lecture on the ten days of Ramadan virtues by Mohammed bin Rashid Housing","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"},{"title":"The executive steps of integrated housing projects through tenders roads and infrastructure services within the housing projects ","description":"The executive steps of integrated housing projects through tenders roads and infrastructure services within the housing projects ","date":"29-Feb-2016","image":"https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/2015-07-08.jpg"}]
     * success : true
     */

    private boolean success;
    private List<PayloadBean> payload;



    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<PayloadBean> getPayload() {
        return payload;
    }

    public void setPayload(List<PayloadBean> payload) {
        this.payload = payload;
    }

    public static class PayloadBean {
        /**
         * title : Mohammed bin Rashid inspects infrastructure projects in the eastern region
         * description : Mohammed bin Rashid inspects infrastructure projects in the eastern region
         * date : 02-Mar-2016
         * image : https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg
         */

        @SerializedName("title")
        private String titleX;
        private String description;
        @SerializedName("date")
        private String dateX;
        @SerializedName("image")
        private String imageX;

        public String getTitleX() {
            return titleX;
        }

        public void setTitleX(String titleX) {
            this.titleX = titleX;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDateX() {
            return dateX;
        }

        public void setDateX(String dateX) {
            this.dateX = dateX;
        }

        public String getImageX() {
            return imageX;
        }

        public void setImageX(String imageX) {
            this.imageX = imageX;
        }
    }
}
