package com.example.myapplication;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {


    EditText edtName,edtMobile,edtEmail,edtEid,edtIdbarahno,edtUnifiednumber;

    Button btnLogin;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtName=(EditText)findViewById(R.id.edtName);
        edtMobile=(EditText)findViewById(R.id.edtMobile);
        edtEmail=(EditText)findViewById(R.id.edtEmail);
        edtEid=(EditText)findViewById(R.id.edtEid);
        edtIdbarahno=(EditText)findViewById(R.id.edtIdbarahno);
        edtUnifiednumber=(EditText)findViewById(R.id.edtUnifiednumber);
        btnLogin=(Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {






                if(edtName.getText().toString().isEmpty()){


                    edtName.setError("Enter Name");
                }else if(edtMobile.getText().toString().isEmpty()){
                    edtMobile.setError("Enter Mobile Number");

                }else if(!new Utility().isValidMobile(edtMobile.getText().toString())){

                    edtMobile.setError("Enter Valid Mobile Number");

                }else if(edtEmail.getText().toString().isEmpty()){

                    edtEmail.setError("Enter Email Number");


                }else if(!new Utility().isValidMail(edtEmail.getText().toString())){


                    edtEmail.setError("Enter Valid Email Number");
                }else if(edtEid.getText().toString().isEmpty()){

                    edtEid.setError("Enter Eid Number");

                }else if(edtIdbarahno.getText().toString().isEmpty()){

                    edtIdbarahno.setError("Enter Idbarahno Number");

                }
                else if(edtUnifiednumber.getText().toString().isEmpty()){

                    edtUnifiednumber.setError("Enter Unifiednumber Number");

                }else {

                    User user=new User();

                    user.setEid(edtEid.getText().toString());
                    user.setEmailID(edtEmail.getText().toString());
                    user.setIdbarahno(edtIdbarahno.getText().toString());
                    user.setMobileNo(edtMobile.getText().toString());
                    user.setName(edtName.getText().toString());
                    user.setUnifiednumber(edtUnifiednumber.getText().toString());

                    doLogin(user);
                }





            }
        });



    }


    //Funtion to make Login
    private void doLogin(final User user) {




        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                "https://api.qa.mrhe.gov.ae/mrhecloud/v1.4/api/iskan/v1/certificates/towhomitmayconcern", null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("Mohan",response.toString());


                        //Moving Next Screen because of facing Failure response of while making login

                        Intent i=new Intent(MainActivity.this,ListNewsActivity.class);
                        startActivity(i);


                    }
                }, new Response.ErrorListener() {



            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("Mohan2",error.toString());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("consumer-key", "mobile_dev");
                headers.put("consumer-secret", "20891a1b4504ddc33d42501f9c8d2215fbe85008");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("eid", user.getEid());
                params.put("name", user.getName());
                params.put("idbarahno", user.getIdbarahno());
                params.put("emailaddress", user.getEmailID());
                params.put("unifiednumber", user.getUnifiednumber());
                params.put("mobileno", user.getMobileNo());

                return params;
            }

        };

// Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, "Login");
    }
}
