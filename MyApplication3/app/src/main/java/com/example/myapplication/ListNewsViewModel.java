package com.example.myapplication;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.myapplication.AppController.TAG;

public class ListNewsViewModel extends ViewModel {

    //this is the data that we will fetch asynchronously
    private MutableLiveData<List<ListnewsModel>> newsList;

    public static String API="https://api.qa.mrhe.gov.ae/mrhecloud/v1.4/api/public/v1/news?local=en";

    public Gson gson;


    //we will call this method to get the data
    public MutableLiveData<List<ListnewsModel>> getNews() {
        //if the list is null
        if (newsList == null) {
            newsList = new MutableLiveData<List<ListnewsModel>>();
            //we will load it asynchronously from server in this method
             loadListNews();
        }
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();
        return newsList;
    }

    private void loadListNews() {

        StringRequest strReq = new StringRequest(Request.Method.GET,
                API, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("Mohan", response.toString());
                newsList.setValue( Arrays.asList(gson.fromJson(response,ListnewsModel.class)));




            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

            }
        }){


            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("consumer-key", "mobile_dev");
                headers.put("consumer-secret", "20891a1b4504ddc33d42501f9c8d2215fbe85008");
                return headers;
            }
        };


// Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, "Mohan");
    }





    
    
    


}
