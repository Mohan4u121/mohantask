package com.example.myapplication;

public class User {

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getIdbarahno() {
        return idbarahno;
    }

    public void setIdbarahno(String idbarahno) {
        this.idbarahno = idbarahno;
    }

    public String getUnifiednumber() {
        return unifiednumber;
    }

    public void setUnifiednumber(String unifiednumber) {
        this.unifiednumber = unifiednumber;
    }

    String emailID,mobileNo,name;
    String eid,idbarahno,unifiednumber;

}
